#include <stdio.h>
#include "scheduler.h"

void fcfs(struct process *head)
{
	struct process *current = head->next;

	while (current != head)
	{
		if (current->state == 0)
		{
			current = current->next;
		}
		else if(current->state == 2)
		{
			current->state = 1;

			return;
		}
		else if(current->state == 1 && current->cycles_todo == 0)
		{
			current->state = 0;
			current = current->next;
		}
		else
		{
			return;
		}
	}
	return;
}
void cleanup(struct process *head) {

    for (struct process *c = head->next;
        c != head;
        c = c->next){
            if (c->state == PS_RUNNING){
                if (c->cycles_todo > 0){
                    c->state = PS_READY;
                }
                else {
                    c->state = PS_DEAD;
                }
                
                
            }
        }
}
void spn(struct process *head)
{
	struct process *current = head->next;
	struct process *stored = head->next; // storedAdressOfTheElementWithTheSmallestCycleToDo

	while (current != head)
	{
		if (current->state == 0)
		{
			current = current->next;
		}
		else if(current->state == 2)
		{
			if (current->cycles_todo < stored->cycles_todo || stored->cycles_todo == 0)
				stored = current;

			current = current->next;
		}
		else if(current->state == 1 && current->cycles_todo > 0) // cou
		{
			return;
		}
		else
		{
			current->state = 0;
			current = current->next;
		}
	}
	if(stored != head && stored->cycles_todo != 0 && stored->state == 2)
		stored->state = 1;
	return;
}

void srt(struct process *head) // shortest remaining Time
{
	struct process *current = head->next;
	struct process *stored = head->next; // storedAdressOfTheElementWithTheSmallestCycleToDo

	while (current != head)
	{
		if (current->state == 0)
		{
			current = current->next;
		}
		else if(current->state == 2)
		{
			if (current->cycles_todo < stored->cycles_todo || stored->cycles_todo == 0)
				stored = current;
			current = current->next;
		}
		else if(current->state == 1 && current->cycles_todo == 0)
		{
			current->state = 0;
			current = current->next;
		}
		else
		{
			if (stored->cycles_todo == 0)
			{
				stored = current;
				current->state = 2;
			}
			else if (stored->cycles_todo < current->cycles_todo)	
			{
				current->state = 2;
			} else
				stored = current;
			current = current->next;
		}
	}
	if(stored != head && stored->cycles_todo != 0 && stored->state != 0)
		stored->state = 1;
	return;
}

struct process *last_running;

void rr(struct process *head){

	if(last_running == NULL) {
		last_running = head;
	}

	cleanup(head);

	for (struct process *c = last_running->next;
			c != last_running;
			c = c->next) {

		if (c == head) { 
			c=c->next;
		}

		if (c->state == PS_READY){
			c->state = PS_RUNNING;
			last_running = c;
			return;
		}

	}
}
//void rr(struct process *head) // shortest remaining Time
//{
//	struct process *current = head->next;
//	struct process *stored = head->next; // storedAdressOfTheElementWithTheSmallestCycleToDo
//	while (current != head)
//	{
//		if (current->state == 0)
//		{
//			current = current->next;
//		}
//		else if(current->state == 2)
//		{
//			printf("READYbegin: current->cycles_todo is %d. stored->cycles_todo is %d\n", current->cycles_todo, stored->cycles_todo);
//			stored = current;
//			head->prev->state = 2;
//			head->prev->next = current;
//			head-> next = current->next;	
//			current->next = head;
//			current->prev = head->prev;
//		      	head->prev = current;
//			printf("READYend: current->cycles_todo is %d. stored->cycles_todo is %d\n", current->cycles_todo, stored->cycles_todo);
//			break;
//		}
//	}
//	if(stored != head && stored->cycles_todo != 0 && stored->state != 0)
//		stored->state = 1;
//	return;
//}
void hrrn(struct process *head) // High response ...
{
	struct process *current = head->next;
	struct process *stored = head->next; // storedAdressOfTheElementWithTheSmallestCycleToDo

	while (current != head)
	{
		if (current->state == 0)
		{
			current = current->next;
		}
		else if(current->state == 2)
		{
			if ((current->cycles_todo != 0 && stored->cycles_todo != 0 && ((1.0 + (current->cycles_waited / current->cycles_todo)) > (1.0 + (stored->cycles_waited / stored->cycles_todo)))) || stored->cycles_todo == 0)
				stored = current;
			current = current->next;
		}
		else if(current->state == 1 && current->cycles_todo == 0)
		{
			current->state = 0;
			current = current->next;
		}
		else
		{
			if (stored->cycles_todo == 0)
			{
				stored = current;
				current->state = 2;
			}
			else if (current->cycles_todo!= 0 &&  stored->cycles_todo != 0 && (1.0 + (current->cycles_waited/current->cycles_todo)) < (1.0 + (stored->cycles_waited/stored->cycles_todo)))	
			{
				current->state = 2;
			} else
				stored = current;
			current = current->next;
		}
	}
	if(stored != head && stored->cycles_todo != 0 && stored->state != 0)
		stored->state = 1;
	return;
}
