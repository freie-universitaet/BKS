#define _POSIX_C_SOURCE 199309L

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>

enum {SECS_TO_SLEEP = 2, NSEC_TO_SLEEP = 0};

void my_signal_handler(int signal_number)
{
	(void)signal_number;
}

int ft_putnbr(int nb)
{
	unsigned int	nbr;
	int err;// unecessary use of err but avoid error message
	if (nb < 0)
	{
		int x = '-';
		err = write(1,&x,1);// unecessary use of err but avoid error message
		nbr = nb * -1;
	}
	else
		nbr = nb;
	if (nbr >= 10)
		ft_putnbr(nbr / 10);
	nbr = nbr % 10 + 48;
	err = write(1, &nbr, 1);// unecessary use of err but avoid error message
	return err;// unecessary use of err but avoid error message
}

int main(void) {
	struct timespec remaining, request = {SECS_TO_SLEEP, NSEC_TO_SLEEP};

	struct sigaction sa_register = {
		.sa_handler = &my_signal_handler,
		.sa_flags = 0,
	};
	char *boseSatz = "Lulz, u suck! Try again !!\n";
	char *satzFurId = "getpid = ";
	int id = getpid();
	int i = 0;
	int err_write;// unecessary use of err_write but avoid error message
	char return_carriage = '\n';
	
	sigemptyset(&sa_register.sa_mask);
	while(satzFurId[i])
	{
		err_write = write(1, &satzFurId[i], 1);// unecessary use of err_write but avoid error message
		i++;
	}
	ft_putnbr(id);
	err_write = write(1, &return_carriage, 1);// unecessary use of err_write but avoid error message
	i = 0;
	int err = sigaction(SIGINT, &sa_register, NULL);
	// error handling recommended but ommitted here.
	while (1) {
		while(boseSatz[i])
		{
			err_write = write(1, &boseSatz[i], 1);// unecessary use of err_write but avoid error message
			i++;
		}
		i = 0;
		nanosleep(&request, &remaining);
	}
	i = err_write;// unecessary use of err_write but avoid error message
	exit(err);
}

