#define _POSIX_SOURCE
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc ,char **argv)
{
	if(argc < 2)
	{
		printf("usage : ./kill PID");
		return -1;
	}
	kill(atoi(argv[1]),SIGKILL);
	return 0;
}

