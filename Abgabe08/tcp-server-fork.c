#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#define LENGTH 127

//server

int main(int argc, char **argv){
	char *ip_address = argv[1];
	int port_number = strtol(argv[2], NULL, 10);
	int server_fd = -1, new_socket = -1, val_read = -1;
	struct sockaddr_in address;
	int addr_length = sizeof(address);
	char buffer[LENGTH];


	if(argc < 3)
	{
		perror("No given port, No given ip_address");
		return EXIT_FAILURE;
	}
	// printf("%s %d", ip_address, port_number);
	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
		perror("socket()");
		return EXIT_FAILURE;
	}
	address.sin_family = AF_INET;
	address.sin_port = htons(port_number); 
	if (inet_pton(AF_INET , ip_address, &address.sin_addr.s_addr) != 1)
	{
		perror("inet_pton");
		return EXIT_FAILURE;
	}

	if (bind(server_fd, (struct sockaddr *)&address, addr_length) < 0){
		perror("bind()");
		return EXIT_FAILURE;
	}

	const int max_nbr_client = 3;

	if ((listen(server_fd, max_nbr_client)) < 0){
		perror("listen");
		return EXIT_FAILURE;
	}

	puts("quit with 'CTRL+C'");
	while(true)
	{
		if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addr_length)) < 0){
			perror("accept()");
			return EXIT_FAILURE;
		}
		// puts("Connected with new socket\n");
		pid_t pid = fork(); // fork is always called ... also in the child when we don't need it ....
		if(pid == 0)// who sind die pid gespeichert ?
		{
			// child functionality here ...
			while(new_socket){
				if ((val_read = read(new_socket, buffer, LENGTH)) < 0){
					perror("read()");
					return EXIT_FAILURE;
				}

				char answer[] = "OK";
				if (send(new_socket, answer, strlen(answer), 0) < 0){
					perror("send()");
					return EXIT_FAILURE;
				}
				// puts("The message received from the client is:");
				// puts(buffer);
				if (!strncmp(buffer, "quit", 4))
				{
					if(close(new_socket) < 0){
						perror("close()");
						memset(buffer, 0, sizeof(buffer));
					}
					new_socket = 0;
					exit(0);
				}
				memset(buffer, 0, 127);
				bzero(buffer, 127);
			}
		}
		else 
		{
			// parent functionality here ...
			// nothing to do ...
			// while(waitpid(-1, NULL, WNOHANG) > 0); <-- this is useless
		}
	}
	return EXIT_SUCCESS;
}
