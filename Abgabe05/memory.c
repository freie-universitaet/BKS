#include "memory.h"
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

static char memory[MEM_SIZE];

struct mb {
	bool free;
	int value;
	struct mb *next;
	struct mb *prev;
};

struct mb *head = (struct mb*) (&memory[0]); 
struct mb *dummy = (struct mb*) (&memory[MEM_SIZE-sizeof(struct mb)]);

void memory_init(void)
{
	dummy->next = head;
	dummy->prev = head;

	head->next = dummy;
	head->prev = dummy;
	head->free = true;
	head->value = 0;
}

bool isEnoughMemorySpace(size_t byte_count)
{
	struct mb *selected = head;
	int acc = 0;
	int howManyFreeBlock = 0;
	while(selected != dummy)
	{
		if(selected->free)
		{
			acc += (char *) (selected->next) - (char *) (selected) - sizeof(struct mb);
			howManyFreeBlock++;
		}
		selected = selected->next;
	}
	if (byte_count + sizeof(struct mb) < acc + sizeof(struct mb) * (howManyFreeBlock - 1))
	{
		return true;
	}
	else
		return false;
}

void *buildNewBlockAtPositionBasedOn(struct mb* selected, struct mb * selectedNext, struct mb * selectedPrev, int newPosition)
{

	struct mb* newBlock = (struct mb*) (&memory[newPosition]);
	newBlock->free = selected->free; 
	newBlock->prev = selectedPrev;
	newBlock->next = selectedNext;
	selectedNext->prev = newBlock;
	selectedPrev->next = newBlock;
	return(newBlock);
}

void shiftPosition(int range, struct mb *selected)
{
	while(selected != dummy)
	{
		int newPosition = (char *) (selected) - range - (char *) (head) - sizeof(struct mb); 
		selected = buildNewBlockAtPositionBasedOn(selected, selected->next, selected->prev, newPosition);
		selected = selected->next;
	}
}

void clearMemory()
{
	struct mb *selected = head;

	while (selected != dummy)
	{
		if (selected->free == true && selected->next->next == dummy)
		{
			selected->next = dummy;
			selected->next->prev = selected;
		}
		else if (selected->next == dummy) // is needed otherwise we do wrong manipulation on the last mb.
		{
		}
		else if (selected->free == true)
		{
		int range = (char *) (selected->next) - (char*) (selected) -sizeof(struct mb);
		int newPosition = ((char *) (selected->next->next) - (char *) (head) - range ) - sizeof(struct mb);
		selected->free=false;
		struct mb * newBlock = buildNewBlockAtPositionBasedOn(selected->next->next, selected->next->next->next, selected, newPosition);
		shiftPosition(range, newBlock->next);
		}
		selected = selected->next;
	}
}

void *memory_allocate(size_t byte_count)
{
	if(isEnoughMemorySpace(byte_count))
	{
		int position = (char *) (dummy->prev) + 3 * sizeof(struct mb) - (char *) (head);
		int toCheck = position + byte_count;
		if (toCheck > MEM_SIZE)
		{
			clearMemory();
		}
		position = (char *) (dummy->prev) + sizeof(struct mb) - (char *) (head);
		struct mb *newBlock = (struct mb*) (&memory[position + byte_count]);
		dummy->prev->next = newBlock;
		dummy->prev->free=false;
		newBlock->prev = dummy->prev;
		newBlock->next = dummy;
		newBlock->free = true;
		newBlock->value = 1 + newBlock->prev->value;
		dummy->prev = newBlock;
		return(newBlock);
	}
	else
		return NULL;
}

void memory_free(const void *const pointer)
{
	struct mb* selected = (struct mb*) (pointer);
	selected->prev->free = true;
}

void memory_print(void)
{
	struct mb *selected = head;
	int blockSize;
	int i = 1;
	while(selected != dummy)
	{
		blockSize = (char*) (selected->next) - sizeof(struct mb) - (char*) (selected);
		if(selected->free)
			printf("Block %d of size %d (FREE).\n", i, blockSize);
		else
			printf("Block %d of size %d (USED).\n", i, blockSize);
		selected = selected->next;
		i++;
	}
}

void *memory_by_index(size_t index)
{
	struct mb *selected= head;
	while(selected != dummy && index != 0)
	{
		selected = selected->next;
		index--;
	}
	if (selected == dummy || index != 0)
		return NULL;
	else
		return selected;
}
