#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define LENGTH 127

static const unsigned int port_number = 3451;
//server

int main(void){
	int server_fd = -1, new_socket = -1, val_read = -1;
	struct sockaddr_in address;
	int addr_length = sizeof(address);
	char buffer[LENGTH];

	if ((server_fd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0){
		perror("socket()");
		return EXIT_FAILURE;
	}

	address.sin_family = AF_UNIX;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(port_number);

	if (bind(server_fd, (struct sockaddr *)&address, addr_length) < 0){
		perror("bind()");
		return EXIT_FAILURE;
	}

	const int max_nbr_client = 3;

	if ((listen(server_fd, max_nbr_client)) < 0){
		perror("listen");
		return EXIT_FAILURE;
	}

	puts("quit with 'CTRL+C'");
	while(true)
	{
		if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addr_length)) < 0){
			perror("accept()");
			return EXIT_FAILURE;
		}
		// puts("Connected with new socket\n");
		while(new_socket){
			if ((val_read = read(new_socket, buffer, LENGTH)) < 0){
				perror("read()");
				return EXIT_FAILURE;
			}

			char answer[] = "OK";
			if (send(new_socket, answer, strlen(answer), 0) < 0){
				perror("send()");
				return EXIT_FAILURE;
			}
			// puts("The message received from the client is:");
			// puts(buffer);
			if (!strncmp(buffer, "quit", 4))
			{
				if(close(new_socket) < 0){
					perror("close()");
					memset(buffer, 0, sizeof(buffer));
				}
				new_socket = 0;
			}
			memset(buffer, 0, 127);
			bzero(buffer, 127);
		}

	}

	return EXIT_SUCCESS;
}
