#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <stdbool.h>
#include <netinet/in.h>

#define LENGTH 127

//client

static const unsigned int port_number = 3451;

int main(int argc, char **argv){

	if (argc != 2){
		fprintf(stderr, "usage: %s <CLIENT_ID> \n", argv[0]);
		return EXIT_FAILURE;
	}

	int client_fd = -1, val_read = -1;
	struct sockaddr_in serv_addr;
	char *buffer = malloc(sizeof(char) * LENGTH);
	char *basic_content = malloc(sizeof(char) * LENGTH);

	memset(buffer, 0, LENGTH);
	memset(basic_content, 0, LENGTH);

	if ((client_fd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0){
		perror("socket()");
		return EXIT_FAILURE;
	}

	serv_addr.sin_family = AF_UNIX;
	serv_addr.sin_port = htons(port_number);

	if (connect(client_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
		perror("connect()");
		return EXIT_FAILURE;
	}

	while(true){
		printf("enter something: ");
		memset(buffer, 0, strlen(buffer));
		buffer = fgets(buffer, LENGTH, stdin);
		int buffer_length = strlen(buffer);
		buffer[buffer_length - 1] = '\0';

		strcat(basic_content, "");
		memcpy(basic_content, buffer, strlen(buffer));
		if (send(client_fd, basic_content, strlen(basic_content), 0) < 0){
			perror("send()");
			return EXIT_FAILURE;
		}
		puts(buffer);
		puts("message send");
		bzero(buffer, LENGTH);

		if ((val_read = read(client_fd, buffer, LENGTH)) < 0){
			perror("read()");
			return EXIT_FAILURE;
		}
		puts(buffer);
		if ((!strncmp(basic_content, "quit", 4) && !strncmp(buffer, "OK", 2)))
		{
			if(close(client_fd) < 0) {
				perror("close");
			}
			bzero(buffer, LENGTH);
			memset(basic_content, 0, LENGTH);
			return EXIT_SUCCESS;
		}
		memset(buffer, 0, LENGTH);
		bzero(basic_content, 127);
		memset(basic_content, 0, LENGTH);
	}



	free(buffer);
	free(basic_content);
	return EXIT_SUCCESS;
}

