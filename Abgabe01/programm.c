#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
int copy(int srcfd, int destfd)
{
	while (1)
	{
		char buffer[1024];
		
		ssize_t nread = read(srcfd, buffer, 1024);
		if (nread == 0)
		{
			return(0);
		}
		int nwritten = 0;
		while (nwritten < nread)
		{
			int nwritten1 = write(destfd, buffer+nwritten, nread-nwritten);
			nwritten = nwritten + nwritten1;
		}
		return(0);
	}
}

int main(int argc, char *argv[])
{
	if (argc == 1)
	{
		int err= copy(0,1);
		return (err);
	}
	int status = 0;
	for (int i = 1; i <argc; i++){
		int srcfd = open(argv[i], O_RDONLY);
		if (srcfd == -1){
			perror(argv[i]);
			status = 1;
		}
		int err = copy(srcfd, 1);
		if (err) 
		{
			status = 1;
			break;
		}
	}
	return(status);
}
