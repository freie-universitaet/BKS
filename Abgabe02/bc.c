#include <stdio.h>
#include <string.h>


int process_file(FILE *input)
{
	int status = 0;
	int acc;
	int x;
	char op;
	int n;

	while(!status)
	{
		n = fscanf(input, "%d %c %d",&acc, &op, &x);
		if (n != 3)
		{
			return(1);	
		}	
		else
		{
			switch (op)
			{
				case '+':
					acc += x;
					break;
				case '-':
					acc -= x;
					break;
				case '*':
					acc *= x;
					break;
				case '/':
					acc /= x;
					break;
				default:
					return(0);
			}
		}
		printf("%d\n", acc);
	}
		return(0);	
}

int main()
{
	process_file(stdin);
	return(1);
}

