# PROCESS

In a first terminal type:
```bin
make
./httpd 127.0.0.1 5000
```

in another terminal type:
```bin
./fclient 123 127.0.0.1 5005
```

It will be asked to you to write something in the prompt, you can write:
```bin
GET /index.html HTTP/1.0 Host: 127.0.0.1
``` 

