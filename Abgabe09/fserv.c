#define _POSIX_C_SOURCE 199309L

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#define LENGTH 127

// Include for cat

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

// copy function from cat
int copy(int srcfd, int destfd)
{
	while (1)
	{
		char buffer[1024];

		ssize_t nread = read(srcfd, buffer, 1024);
		if (nread == 0)
		{
			return(0);
		}
		int nwritten = 0;
		while (nwritten < nread)
		{
			int nwritten1 = write(destfd, buffer+nwritten, nread-nwritten);
			nwritten = nwritten + nwritten1;
		}
		return(0);
	}
}
//server

int main(int argc, char **argv){
	int server_fd, new_socket, val_read;

	if(argc < 3)
	{
		perror("No given port, No given ip_address");
		return EXIT_FAILURE;
	}
	char *ip_address = argv[1];
	int port_number = strtol(argv[2], NULL, 10);
	// printf("%s %d", ip_address, port_number);
	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
		perror("socket()");
		return EXIT_FAILURE;
	}
	struct sockaddr_in address = {
		.sin_family = AF_INET,
		.sin_port = htons(port_number),
	};
	int addr_length = sizeof(address);
	if (inet_pton(AF_INET , ip_address, &address.sin_addr.s_addr) < 0)
	{
		perror("inet_pton");
		return EXIT_FAILURE;
	}

	if (bind(server_fd, (struct sockaddr *)&address, addr_length) < 0){
		perror("bind()");
		return EXIT_FAILURE;
	}

	const int max_nbr_client = 3;

	if ((listen(server_fd, max_nbr_client)) < 0){
		perror("listen");
		return EXIT_FAILURE;
	}

	puts("quit with 'CTRL+C'");
	while(true)
	{
		if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addr_length)) < 0){
			perror("accept()");
			return EXIT_FAILURE;
		}
		// puts("Connected with new socket\n");
		pid_t pid = fork();
		if(pid == 0)
		{
			// child functionality here ...
			while(new_socket){
				char buffer[LENGTH];
				char basic_content[LENGTH];
				memset(buffer, 0, LENGTH);
				if ((val_read = read(new_socket, buffer, LENGTH)) < 0){
					perror("read()");
					return EXIT_FAILURE;
				}
				memcpy(basic_content, buffer, val_read); 
				strcat(basic_content, "\0");
				int srcfd = open(basic_content, O_RDONLY);
				if (srcfd == -1){
					char answer[] = "Nachricht bekommen, aber kein Datei gefunden!";
					if (send(new_socket, answer, strlen(answer), 0) < 0){
						perror("send()");
						return EXIT_FAILURE;
					}
					perror(buffer);
				}
				else
				{
					char answer[] = "Nachricht bekommen, und ein Datei gefunden ! Die Inhalt der Datei ist:\n";
					if (send(new_socket, answer, strlen(answer), 0) < 0){
						perror("send()");
						return EXIT_FAILURE;
					}
					if((val_read = read(new_socket, buffer, LENGTH)) < 0) 
					{
						perror("read()");
						return EXIT_FAILURE;
					}
					// puts("The message received from the client is:");
					puts(buffer);
					int err = copy(srcfd, new_socket);
					if (err) 
					{
						break;
					}
				}
				if (!strncmp(buffer, "quit", 4))
				{
					puts("connection has been closed");
					if(close(new_socket) < 0){
						perror("close()");
						memset(buffer, 0, sizeof(buffer));
					}
					new_socket = 0;
					exit(0);
				}
				memset(buffer, 0, 127);
			}
		}
		else 
		{
			// parent functionality here ...
			// nothing to do ...
			// while(waitpid(-1, NULL, WNOHANG) > 0); <-- this is useless
		}
	}
	return EXIT_SUCCESS;
}

