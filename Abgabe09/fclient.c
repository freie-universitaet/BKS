#define _POSIX_C_SOURCE 199309L

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <stdbool.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define LENGTH 127

//client

// Include for cat

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

// copy function from cat
int copy(int srcfd, int destfd)
{
	while (1)
	{
		char buffer[1024];

		ssize_t nread = read(srcfd, buffer, 1024);
		if (nread == 0)
		{
			return(0);
		}
		int nwritten = 0;
		while (nwritten < nread)
		{
			int nwritten1 = write(destfd, buffer+nwritten, nread-nwritten);
			nwritten = nwritten + nwritten1;
		}
		return(0);
	}
}


int main(int argc, char **argv)
{
	if (argc != 4)
	{
		fprintf(stderr, "usage: %s <CLIENT_ID>,or no ip_address or no port. \n", argv[0]);
		return EXIT_FAILURE;
	}
	int port_number = strtol(argv[3], NULL, 10);
	char *ip_address = argv[2];
	int client_fd = -1, val_read = -1;
	struct sockaddr_in serv_addr;
	char *buffer = malloc(sizeof(char) * LENGTH);
	char *basic_content = malloc(sizeof(char) * LENGTH);

	memset(buffer, 0, LENGTH);
	memset(basic_content, 0, LENGTH);

	if ((client_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		perror("socket()");
		return EXIT_FAILURE;
	}

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(port_number);
	if (inet_pton(AF_INET, ip_address, &serv_addr.sin_addr.s_addr) < 0 )
	{
		perror("failed by inet_pton function");
		return EXIT_FAILURE;
	}

	if (connect(client_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
	{
		perror("connect()");
		return EXIT_FAILURE;
	}

	while(1)
	{
		puts("enter something: ");
		memset(buffer, 0, strlen(buffer));
		ssize_t nread = read(0, buffer, 127);
		if (nread == 0)
			return(0);
		memset(basic_content, 0, strlen(basic_content));
		memcpy(basic_content, buffer, strlen(buffer));
		// puts(buffer);
		if (send(client_fd, buffer, nread, 0) < 0){
			perror("send()");
			return EXIT_FAILURE;
		}
		puts("message send");
		memset(buffer, 0, strlen(buffer));

		if ((val_read = read(client_fd, buffer, LENGTH)) < 0){
			perror("read()");
			return EXIT_FAILURE;
		}
		if ((!strncmp(buffer, "Nachricht bekommen, und ein Datei gefunden ! Die Inhalt der Datei ist:\n", val_read)))
		{
			if (send(client_fd, buffer, nread, 0) < 0){
				perror("send()");
				return EXIT_FAILURE;
			}
			memset(buffer, 0, LENGTH);
			memset(basic_content, 0, LENGTH);
			if ((val_read = read(client_fd, buffer, LENGTH)) < 0){
				perror("read()");
				return EXIT_FAILURE;
			}
			int err = write(1, buffer, val_read);
			if (err < 0)
				break;
		}
		if ((!strncmp(basic_content, "quit", 4)))
		{
			if(close(client_fd) < 0) {
				perror("close");
			}
			memset(buffer, 0, LENGTH);
			memset(basic_content, 0, LENGTH);
			free(buffer);
			free(basic_content);
			return EXIT_SUCCESS;
		}
		memset(buffer, 0, LENGTH);
		memset(basic_content, 0, LENGTH);
	}
	free(buffer);
	free(basic_content);
	return EXIT_SUCCESS;
}

