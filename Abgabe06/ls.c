#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <time.h>
#include <string.h>

void fromTime_tToString(struct stat metaData, char *timeAsString)
{
	struct tm *time;
	time = localtime(&metaData.st_mtime);
	strftime(timeAsString, 200, "%a %b %e %H:%M:%S %Y", time);
}

int main(int argc, char **argv)
{
	int n, c;
	int i = 0;
	int addInfo = 0;
	int showHiddenFiles = 0;
	struct dirent **namelist;
	char *folder = NULL;

	struct stat buffer;

	while((c = getopt(argc, argv, "la")) != -1)
	{
		switch(c){
			case 'a':
				showHiddenFiles = 1;
				break;
			case 'l':
				addInfo = 1;
				break;
			case '?':
				fprintf(stderr, "Unrecognized option: '-%c'\n", optopt);
				break;
		}
		folder = argv[optind];
		if (!folder)
			folder = "./";
	}
	if(!folder && argv[1])
		folder = argv[1];
	else if(!folder)
		folder = "./";
	char timeLastModification[200];
	n = scandir(folder, &namelist, 0, alphasort);
	if (n < 0)
		perror("scandir");
	else 
	{
		while(i < n)
		{
			c = stat(namelist[i]->d_name, &buffer);
			fromTime_tToString(buffer, timeLastModification);
			if(showHiddenFiles == 1 || namelist[i]->d_name[0] != '.')
			{
				if(addInfo)
					printf("%s ", timeLastModification); 
				printf("%s\n", namelist[i]->d_name);
			}
			i++;
		}
	}
	return (1);
}
